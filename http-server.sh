#!/bin/bash

port=$1
CGI="$2"

if test -z "$CGI"; then
  echo "
  Simple BASH HTTP Server

  Usage:
  \$ $(basename "$0") <port> <CGI script>

  The CGI script will receive this environment variables:
    * REQUEST_METHOD  whith GET, POST, or othrt HTTP method
    * REQUEST_PATH    the adderess on the browser, but the domain
    * HTTP_HEADER     The full HTTP Header
  "
  exit 1
fi

# Inspirated by
# http://www.linuxscrew.com/2007/09/06/web-server-on-bash-in-one-line

while [ $? -eq 0 ]; do nc -vlp $port -c'
  CGI="'"$CGI"'"
  read method path protocol
  #echo ":: $method \n:: $path \n:: $protocol" >&2
  header=""
  while test -n "$header_line" -o -z "$header"; do
    read header_line
    header_line="$( echo "$header_line" | sed "s/[\r\n]*\$//g" )"
    test -n "$header_line" && header="$header\n$header_line"
    #echo ">> [$header_line]" >&2
  done
  REQUEST_METHOD=$method REQUEST_PATH="$path" PROTOCOL=$protocol \
  HTTP_HEADER="$method $path $protocol\n$header" "$CGI"
  errnum=$?
  if [ $errnum != 0 ]; then
    echo ">> $errnum" >&2
    echo "HTTP/1.0 500 Internal Server Error\nContent-Type: text/html\n
    <html>
    <head><title>500 &mdash; Internal Server Error</title></head>
    <body>
      <h1>500 &mdash; Internal Server Error</h1> $(date)
      <ul><li>CGI: $CGI</li><li>path: $path</li><li>error: $errnum</li></ul>
    </body>
    </html>"
  fi';
done
